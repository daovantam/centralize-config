FROM openjdk:8
ADD target/centralize-config.jar centralize-config.jar
EXPOSE 8888
EntryPoint ["java", "-jar", "centralize-config.jar"]